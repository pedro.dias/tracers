import { terser } from "rollup-plugin-terser";

export default {
  input: 'src/tracers.js',
  output: {
    format: 'umd',
    name: 'Tracers',
    file: 'tracers.min.js',
    dir: 'dist',
    globals: {
      underscore: '_',
      d3: 'd3',
      leaflet: 'L',
      backbone: 'Backbone',
      when: 'when',
    },
  },
  external: [
    'underscore',
    'backbone',
    'leaflet',
    'd3',
    'when',
  ],
  plugins: [terser()],
}
