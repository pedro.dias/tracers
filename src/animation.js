/**
 * @license
 * earth - a project to visualize global air data.
 *
 * Copyright (c) 2014 Cameron Beccario
 * The MIT License - http://opensource.org/licenses/MIT
 *
 * https://github.com/cambecc/earth
 *
 * Changes made by Pedro Dias <pedrodias.miguel@gmail.com>, 2018:
 *  * Components split into multiple files
 *  * Adopted es6 syntax
 *  * Added leaflet integration
 */

import * as micro from './micro.js';


var INTENSITY_SCALE_STEP = 10;
var PARTICLE_MULTIPLIER = 2;
var MAX_PARTICLE_AGE = 20;
var PARTICLE_LINE_WIDTH = 1.4;
var FRAME_RATE = 30;
var NULL_WIND_VECTOR = [NaN, NaN, null];
var HOLE_VECTOR = [NaN, NaN, null];


export function animate(globe, field, grids, context) {
  var cancel = this.cancel;

  var bounds = globe;
  var colorStyles = micro.windIntensityColorScale(
    INTENSITY_SCALE_STEP,
    grids.primaryGrid.particles.maxIntensity,
  );
  var buckets = colorStyles.map(function() { return []; });
  var particleCount = Math.round(bounds.width * PARTICLE_MULTIPLIER);
  var fadeFillStyle = 'rgba(0, 0, 0, 0.80)';

  var particles = [];
  for(var i = 0; i < particleCount; i++) {
    particles.push(field.randomize({age: _.random(0, MAX_PARTICLE_AGE)}));
  }

  function evolve() {
    buckets.forEach(function(bucket) { bucket.length = 0; });
    particles.forEach(function(particle) {
      if(particle.age > MAX_PARTICLE_AGE) {
        field.randomize(particle).age = 0;
      }

      var x = particle.x;
      var y = particle.y;
      var v = field(x, y);
      var m = v[2];

      if(m === null) {
        particle.age = MAX_PARTICLE_AGE;
      } else {
        var xt = x + v[0];
        var yt = y + v[1];
        if(field.isDefined(xt, yt)) {
          particle.xt = xt;
          particle.yt = yt;
          buckets[colorStyles.indexFor(m)].push(particle);
        } else {
          particle.x = xt;
          particle.y = yt;
        }
      }

      particle.age += 1;
    });
  }

  context.lineWidth = PARTICLE_LINE_WIDTH;
  context.fillStyle = fadeFillStyle;

  function draw() {
    var prev = context.globalCompositeOperation;
    context.globalCompositeOperation = 'destination-in';
    context.fillRect(bounds.x, bounds.y, bounds.width, bounds.height);
    context.globalCompositeOperation = prev;

    buckets.forEach(function(bucket, i) {
      if(bucket.length > 0) {
        context.beginPath();
        context.strokeStyle = colorStyles[i];
        bucket.forEach(function(particle) {
          context.moveTo(particle.x, particle.y);
          context.lineTo(particle.xt, particle.yt);
          particle.x = particle.xt;
          particle.y = particle.yt;
        });
        context.stroke();
      }
    });
  }

  (function frame() {
    try {
      if(cancel.requested) {
        field.release();
        return;
      }
      evolve();
      draw();
      setTimeout(frame, FRAME_RATE);
    } catch(e) {
      console.log(e);
    }
  })();
}
