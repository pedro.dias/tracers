/**
 * @license
 * earth - a project to visualize global air data.
 *
 * Copyright (c) 2014 Cameron Beccario
 * The MIT License - http://opensource.org/licenses/MIT
 *
 * https://github.com/cambecc/earth
 *
 * Changes made by Pedro Dias <pedrodias.miguel@gmail.com>, 2018:
 *  * Components split into multiple files
 *  * Adopted es6 syntax
 *  * Added leaflet integration
 */

import * as Backbone from 'backbone';
import * as when from 'when';


export function createAgent(initial) {

  function cancelFactory() {
    return function cancel() {
      cancel.requested = true;
      return agent;
    };
  }

  function runTask(cancel, taskAndArguments) {

    function run(args) {
      return cancel.requested ? null : _.isFunction(task) ? task.apply(agent, args) : task;
    }

    function accept(result) {
      if(!cancel.requested) {
        value = result;
        agent.trigger('update', result, agent);
      }
    }

    function reject(err) {
      if(!cancel.requested) {
        agent.trigger('reject', err, agent);
      }
    }

    function fail(err) {
      agent.trigger('fail', err, agent);
    }

    try {
      var task = taskAndArguments[0];
      when.all(_.rest(taskAndArguments))
        .then(run)
        .then(accept, reject)
        .done(undefined, fail);
      agent.trigger('submit', agent);
    } catch(err) {
      fail(err);
    }
  }

  var value = initial;
  var runTask_debounced = _.debounce(runTask, 0);
  var agent = {

    value: function() {
      return value;
    },

    cancel: cancelFactory(),

    submit: function(task, arg0, arg1, and_so_on) {
      this.cancel();
      runTask_debounced(this.cancel = cancelFactory(), arguments);
      return this;
    }
  };

  return _.extend(agent, Backbone.Events);
}
