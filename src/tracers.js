/**
 * @license
 * earth - a project to visualize global air data.
 *
 * Copyright (c) 2014 Cameron Beccario
 * The MIT License - http://opensource.org/licenses/MIT
 *
 * https://github.com/cambecc/earth
 *
 * Changes made by Pedro Dias <pedrodias.miguel@gmail.com>, 2018:
 *  * Components split into multiple files
 *  * Adopted es6 syntax
 *  * Added leaflet integration
 */

import * as when from 'when';
import * as agent from './agent.js';
import * as products from './products.js';
import * as globe from './globe.js';
import * as field from './field.js';
import * as animation from './animation.js';
import * as Canvas from './canvas.js';
import * as micro from './micro.js';

var MAX_TASK_TIME = 100;
var MIN_SLEEP_TIME = 25;

var NULL_WIND_VECTOR = [NaN, NaN, null];
var HOLE_VECTOR = [NaN, NaN, null];
var TRANSPARENT_BLACK = [0, 0, 0, 0];

var meshAgent = agent.createAgent();
var globeAgent = agent.createAgent();
var gridAgent = agent.createAgent();
var rendererAgent = agent.createAgent();
var fieldAgent = agent.createAgent();
var animatorAgent = agent.createAgent();
var overlayAgent = agent.createAgent();

var target = undefined;
var canvascontext = undefined;
var downloadsInProgress = 0;


function buildGrids(url) {
  var attrs = {
    url: url,
  };

  var cancel = this.cancel;
  downloadsInProgress++;
  var loaded = when.map(products.productsFor(attrs), function(product) {
    return product.load(cancel);
  });

  return when.all(loaded)
    .then(function(products) {
      return {primaryGrid: products[0], overlayGrid: products[1] || products[0]};
    }).ensure(function() {
      downloadsInProgress--;
    });
}


function distort(map, lng, lat, x, y, scale, wind) {
  var u = wind[0] * scale;
  var v = wind[1] * scale;
  //var d = micro.distortion(map, lng, lat, x, y);
  var d = [1, 1, 1, 1];
  //wind[0] = d[0] * u + d[2] * v;
  //wind[1] = d[1] * u + d[3] * v;
  wind[0] = u;
  wind[1] = -v;
  return wind;
}


function interpolateField(map, grids) {

  var primaryGrid = grids.primaryGrid;
  var d = when.defer(), cancel = this.cancel;
  
  var bounds = globeAgent.value();
  var velocityScale = bounds.height * primaryGrid.particles.velocityScale;

  var columns = [];
  var point = [];
  var x = bounds.x;
  var interpolate = primaryGrid.interpolate;

  function interpolateColumn(x) {
    var column = [];
    for(var y = bounds.y; y <= bounds.yMax; y += 2) {
      point[0] = x; point[1] = y; /* (x, y) in pixel coordinates */
      var coord = map.containerPointToLatLng(point); /* (x, y) lat lng coordinates */
      var color = TRANSPARENT_BLACK;
      var wind = null;

      if(coord) {
        var lat = coord.lat, lng = coord.lng;
        if(isFinite(lng)) {
          wind = interpolate(lng, lat);
          if(wind) {
            wind = distort(map, lng, lat, x, y, velocityScale, wind);
          }
        }
      }

      column[y+1] = column[y] = wind || HOLE_VECTOR;
    }
    columns[x+1] = columns[x] = column;
  }

  (function batchInterpolate() {
    try {
      if(!cancel.requested) {
        var start = Date.now();
        while(x < bounds.xMax) {
          interpolateColumn(x);
          x+= 2;
          if((Date.now() - start) > MAX_TASK_TIME) {
            setTimeout(batchInterpolate, MIN_SLEEP_TIME);
            return;
          }
        }
      }
      d.resolve(field.createField(columns, bounds, {}));
    } catch(e) {
      d.reject(e);
    }
  })();

  return d.promise;
}


function startInterpolation() {
  fieldAgent.submit(interpolateField, target, gridAgent.value());
}

function cancelInterpolation() {
  fieldAgent.cancel();
}


function stopCurrentAnimation(clearCanvas) {
  animatorAgent.cancel();
  if(clearCanvas) {
    micro.clearCanvas(canvascontext);
  }
}


function buildGlobe(map, ctx, grids) {
  var view = globe.view(ctx);
  var header = grids.primaryGrid.header;

  window.map = map;
  window.header = header;

  var upperLeft = map.latLngToContainerPoint([header.la1, header.lo1]);
  var lowerRight = map.latLngToContainerPoint([header.la2, header.lo2]);

  var bounds = globe.clampedBounds([
    [upperLeft.x, upperLeft.y],
    [lowerRight.x, lowerRight.y]
  ], view);

  return bounds;
}


export function init(map, context, settings) {
  target = map;
  canvascontext = context;

  settings.on('change:step', function(model, step) {
    stopCurrentAnimation(true);
    if(model.attributes.visible) {
      gridAgent.submit(buildGrids, step, model.attributes.date);
    }
  });

  settings.on('change:date', function(model, date) {
    stopCurrentAnimation(true);
    if(model.attributes.visible) {
      gridAgent.submit(buildGrids, model.attributes.step, date);
    }
  });

  settings.on('change:url', function(model, url) {
    stopCurrentAnimation(true);
    if(model.attributes.visible) {
      gridAgent.submit(buildGrids, url);
    }
  });

  target.on('tracers:start', function(ev) {
    if(settings.attributes.visible) {
      gridAgent.submit(buildGrids, settings.attributes.url);
    }
  });

  target.on('tracers:stop', function(ev) {
    stopCurrentAnimation(true);
  });

  target.on('zoomstart', function(ev) {
    stopCurrentAnimation(true);
  });

  target.on('movestart', function(ev) {
    stopCurrentAnimation(true);
  });

  target.on('zoomend', function(ev) {
    if(settings.attributes.visible) {
      globeAgent.submit(buildGlobe, target, context, gridAgent.value());
    }
  });

  target.on('moveend', function(ev) {
    if(settings.attributes.visible) {
      globeAgent.submit(buildGlobe, target, context, gridAgent.value());
    }
  });

  globeAgent.listenTo(gridAgent, 'update', function(grids) {
    globeAgent.submit(buildGlobe, target, context, grids);
  });

  fieldAgent.listenTo(globeAgent, 'update', startInterpolation);

  fieldAgent.on('fail', console.log);
  fieldAgent.on('reject', console.log);

  animatorAgent.listenTo(fieldAgent, 'update', function(field) {
    animatorAgent.submit(animation.animate, globeAgent.value(), field, gridAgent.value(), context);
  });
  animatorAgent.listenTo(gridAgent, 'submit', stopCurrentAnimation.bind(null, false));
  animatorAgent.listenTo(fieldAgent, 'submit', stopCurrentAnimation.bind(null, false));
  animatorAgent.on('fail', console.log);
  animatorAgent.on('reject', console.log);
}


export function start() {
  target.fire('tracers:start', {});
}

export function stop() {
  target.fire('tracers:stop', {});
}

export var canvas = Canvas.canvas;

export var customCanvas = Canvas.customCanvas;
