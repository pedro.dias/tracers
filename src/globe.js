/**
 * @license
 * earth - a project to visualize global air data.
 *
 * Copyright (c) 2014 Cameron Beccario
 * The MIT License - http://opensource.org/licenses/MIT
 *
 * https://github.com/cambecc/earth
 *
 * Changes made by Pedro Dias <pedrodias.miguel@gmail.com>, 2018:
 *  * Components split into multiple files
 *  * Adopted es6 syntax
 *  * Added leaflet integration
 */

import * as _ from 'underscore';


export function view(ctx) {
  var w = ctx.canvas.clientWidth;
  var h = ctx.canvas.clientHeight;
  return {width: w, height: h};
}


export function ensureNumber(num, fallback) {
  return _.isFinite(num) || num === Infinity || num === -Infinity ? num : fallback;
}


export function clampedBounds(bounds, view) {
  var upperLeft = bounds[0];
  var lowerRight = bounds[1];
  var x = Math.max(Math.floor(ensureNumber(upperLeft[0], 0)), 0);
  var y = Math.max(Math.floor(ensureNumber(upperLeft[1], 0)), 0);
  var xMax = Math.min(Math.ceil(ensureNumber(lowerRight[0], view.width)), view.width - 1);
  var yMax = Math.min(Math.ceil(ensureNumber(lowerRight[1], view.height)), view.height -1);
  return {
    x: x,y: y,
    xMax: xMax, yMax: yMax,
    width: xMax - x + 1,
    height: yMax - y + 1,
  };
}
