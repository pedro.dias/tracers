/**
 * @license
 * earth - a project to visualize global air data.
 *
 * Copyright (c) 2014 Cameron Beccario
 * The MIT License - http://opensource.org/licenses/MIT
 *
 * https://github.com/cambecc/earth
 *
 * Changes made by Pedro Dias <pedrodias.miguel@gmail.com>, 2018:
 *  * Components split into multiple files
 *  * Adopted es6 syntax
 *  * Added leaflet integration
 */

import * as _ from 'underscore';


var NULL_WIND_VECTOR = [NaN, NaN, null];
window.randoms = [];

export function createField(columns, bounds, mask) {

  function field(x, y) {
    var column = columns[Math.round(x)];
    return column && column[Math.round(y)] || NULL_WIND_VECTOR;
  }

  field.isDefined = function(x, y) {
    return field(x, y)[2] !== null;
  }

  field.isInsideBoundary = function(x, y) {
    return field(x, y) !== NULL_WIND_VECTOR;
  }

  field.release = function() {
    columns = [];
  }

  field.randomize = function(o) {
    var x, y;
    var safetyNet = 0;
    do {
      x = Math.round(_.random(bounds.x, bounds.xMax));
      y = Math.round(_.random(bounds.y, bounds.yMax));
    } while(!field.isDefined(x, y) && safetyNet++ < 30);
    o.x = x;
    o.y = y;
    window.randoms.push([x, y]);
    return o;
  }

  field.overlay = mask.imageData;

  return field;
}
