/**
 * @license
 * earth - a project to visualize global air data.
 *
 * Copyright (c) 2014 Cameron Beccario
 * The MIT License - http://opensource.org/licenses/MIT
 *
 * https://github.com/cambecc/earth
 *
 * Changes made by Pedro Dias <pedrodias.miguel@gmail.com>, 2018:
 *  * Components split into multiple files
 *  * Adopted es6 syntax
 *  * Added leaflet integration
 */

import * as L from 'leaflet';

export var Canvas = L.Canvas.extend({
  _update: function() {
    if(this._map._animatingZoom && this._bounds) {
      return;
    }

    this._drawnLayers = {};

    L.Renderer.prototype._update.call(this);

    var b = this._bounds,
        container = this._container,
        size = b.getSize(),
        m = L.Browser.retina ? 2 : 1;

    L.DomUtil.setPosition(container, b.min);

    container.width = m * size.x;
    container.height = m * size.y;
    container.style.width = size.x + 'px';
    container.style.height = size.y + 'px';

    if(L.Browser.retina) {
      this._ctx.scale(2, 2);
    }

    this._ctx.translate(-b.min.x, -b.min.y);

    this.fire('update');
  },
});

export var CustomCanvas = L.Class.extend({
  initialize: function(parentElement) {
    var bbox = parentElement.getBoundingClientRect();
    this.element = L.DomUtil.create('canvas', 'customcanvas', parentElement);
    this.element.width = bbox.width;
    this.element.height = bbox.height;
    this._ctx = this.element.getContext('2d');
  },

  getContext: function() {
    return this._ctx;
  },
});

export function customCanvas(parentElement) {
  return new CustomCanvas(parentElement);
}

export function canvas(opts) {
  return new Canvas(opts);
}
