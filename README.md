# Tracers

Simple visualization of wind data using a field of particles. Inspired by the
library [earth](https://github.com/cambecc/earth).

## Dependencies

* Backbone and all its dependencies (Underscore and JQuery)
* Leaflet
* d3, version 4.13.0
* when, distributed with this library

## Usage

See the demo for a complete example.

```html
<script src="tracers.min.js"></script>
<script>
  var map = L.map(); // Create a normal leaflet map object
  
  // Create the canvas where the particles will be rendered
  var canvas = Tracers.customCanvas(map.getContainer());

  // Initialize the simulation. The settings argument must
  // be a Backbone model with the attributes url and visible.
  // The attribute url is the location from where wind data is
  // retrieved. The attribute visible must be a boolean indicating
  // if the tracers are visible or not.
  Tracers.init(map, canvas.getContext(), settings);

  // Start the simulation
  Tracers.start()

  // Stop the simulation
  Tracers.stop()
</script>
```

If you don't want to use a `Backbone` model for the settings, the container
used must emulate the Event API of `Backbone` models. Under the hood, the Tracers
library uses the events triggered by the model on attribute change to trigger
reload and rendering behaviour.

## Building

We use [rollup](https://rollupjs.org/guide/en) as the build tool for Tracers. If
you want to develop or just build your own version of the library follow this steps:

```bash
$ npm install
$ npm run build
```

## Notes

This library will be deprecated soon.
