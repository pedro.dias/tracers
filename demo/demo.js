var map = L.map('map', {
  zoom: 5,
  center: [38, -8],
});

var baselayer = L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {

});

baselayer.addTo(map);

var canvas = Tracers.customCanvas(map.getContainer());

var SettingsModel = Backbone.Model.extend({
  defaults: {
    url: 'http://mf2.ipma.pt/services/data/ecmwf/wind/2018-11-13T00/12',
    visible: true,
  },
});

var settings = new SettingsModel();

Tracers.init(map, canvas.getContext(), settings);
Tracers.start();
